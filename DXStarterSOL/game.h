#pragma once
#include "dice.h"

class Game {
public:
  Dice dice1_;
  Dice dice2_;
  int throws;
};
