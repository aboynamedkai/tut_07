#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <iomanip>
#include <sstream>


#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "SimpleMath.h"
#include "SpriteFont.h"
#include "rng.h"
#include "dice.h"
#include "score.h"
#include "game.h"
#include "gameData.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

void Score::init()
{
  amount_ = 0;
}
int Score::getAmount() const
{
  return amount_;
}
void Score::updateAmount(int value)
//increment when value>0, decrement otherwise
{
  amount_ += value;
}
//--------end of Score class
void Dice::init()
//reset the random number generator from current system time
{
  rng_.seed();
}
int Dice::getFace() const
//get the value of the dice face
{
  return face_;
}
void Dice::roll()
//roll the dice
{
  face_ = rng_.getRandomValue(GameData::SIXES::SIDES_DICE);
}
//--------end of Dice class (version 2)

/*
Blank solution - setup DX11 and blank the screen
*/



DirectX::SpriteFont *gpFont = nullptr;
DirectX::SpriteBatch *gpSpriteBatch = nullptr;
float gResTimer = 0;
int gFrameCounter = 0;
GameData gdata;






void InitGame(MyD3D& d3d)
{
  gdata.score.init();
  gdata.game.dice1_.init();
  gdata.game.dice2_.init();

	gpSpriteBatch = new SpriteBatch(&d3d.GetDeviceCtx());
	assert(gpSpriteBatch);

	gpFont = new SpriteFont(&d3d.GetDevice(), L"../bin/data/comicSansMS.spritefont");
	assert(gpFont);


}


//any memory or resources we made need releasing at the end
void ReleaseGame()
{
	delete gpFont;
	gpFont = nullptr;


	delete gpSpriteBatch;
	gpSpriteBatch = nullptr;
}

//called over and over, use it to update game logic
void Update(float dTime, MyD3D& d3d)
{ 
  ostringstream oss;
  switch (gdata.mode_)
  {
  case GameData::Mode::PLAYER_NAME:
    gdata.mssg = "Type your name (press enter when complete): ";
    gdata.mssg += gdata.name;
    break;
  case GameData::Mode::THROW_NUMBER:
    gdata.mssg2 = "Enter the number of throws you want (press enter when complete): ";
    gdata.mssg2 += gdata.game.throws;
    break;
  case GameData::Mode::PLAY:
    gdata.game.throws -= 48;
    for (int i = 1; i <= gdata.game.throws; i++)
    {
      gdata.game.dice1_.roll();
      gdata.game.dice2_.roll();
      
      if (gdata.game.dice1_.getFace() == gdata.game.dice2_.getFace())
      {
        gdata.score.updateAmount(gdata.game.dice1_.getFace());
        gdata.finalScore = gdata.score.amount_;
      }
      else
        if ((gdata.game.dice1_.getFace() == 6) || (gdata.game.dice2_.getFace() == 6))
            gdata.score.updateAmount(1);
            gdata.finalScore = gdata.score.amount_;

      oss <<  "  \nIn try no: " << to_string(i) << " the dice values are: " << to_string(gdata.game.dice1_.getFace()) << " & " << to_string(gdata.game.dice2_.getFace()) << " The current score is " << to_string(gdata.score.getAmount());
      gdata.mssg3 = oss.str();
    }
    break;
  case GameData::Mode::EXIT:
    gdata.mssg4 = "\n\n" + gdata.name + "'s final score is " + to_string(gdata.finalScore);
    gdata.mssg5 = "\n\n\nPress esc to exit the game";
    break;
  default:
    assert(false);
  }
}

//called over and over, use it to render things
void Render(float dTime, MyD3D& d3d)
{
  float x = GameData::PRINT_OFFSETX;
  float y = GameData::PRINT_OFFSETY;
  float inc = GameData::PRINT_LINEINC;
	WinUtil& wu = WinUtil::Get();

  d3d.BeginRender({ 0,0,0,1 });
	gpSpriteBatch->Begin();

	Vector2 scrn{ (float)wu.GetData().clientWidth, (float)wu.GetData().clientHeight };

  gdata.mssg;
	Vector2 pos = Vector2(0, 0);
  pos = { x, y +=inc };
	gpFont->DrawString(gpSpriteBatch, gdata.mssg.c_str(), pos);

  gdata.mssg2;
  pos = { x, y +=inc };
	gpFont->DrawString(gpSpriteBatch, gdata.mssg2.c_str(), pos);

  gdata.mssg3;
  pos = { x, y += inc };
  gpFont->DrawString(gpSpriteBatch, gdata.mssg3.c_str(), pos);

  gdata.mssg4;
  pos = { x, y += inc + 200 };
  gpFont->DrawString(gpSpriteBatch, gdata.mssg4.c_str(), pos);

  gdata.mssg5;
  pos = { x, y += inc };
  gpFont->DrawString(gpSpriteBatch, gdata.mssg5.c_str(), pos);


	 
	

	gpSpriteBatch->End();
	d3d.EndRender();
}

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	gResTimer = GetClock() + 2;
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
    break;
    
    case 0x0d:
      if (gdata.mode_ == GameData::Mode::PLAYER_NAME)
      {
        gdata.mode_ = GameData::Mode::THROW_NUMBER;
      }
      else if (gdata.mode_ == GameData::Mode::THROW_NUMBER)
      {
        gdata.mode_ = GameData::Mode::PLAY;
      }

      else if (gdata.mode_ == GameData::Mode::PLAY)
      {
        gdata.mode_ = GameData::Mode::EXIT;
      }
     break;

    case 0x08:
      if (gdata.mode_ == GameData::Mode::PLAYER_NAME)
      {
        gdata.name.pop_back();
      }
      /*else if (gdata.mode_ == GameData::Mode::THROW_NUMBER)
      {
        to_string(gdata.game.throws).pop_back();
      }*/
      break;

    case 0x20:
      if (gdata.mode_ == GameData::Mode::PLAYER_NAME)
      {
        gdata.name.push_back(' ');
      }
      break;

    default:

      if (isalpha(wParam))
      {
        gdata.name += wParam;
      }

      if (isdigit(wParam))
      {
        gdata.game.throws += wParam;
      }
      
      break;
			return 0;
		}
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(1024), h(768);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Sixes", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	InitGame(d3d);

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			Update(dTime, d3d);
			Render(dTime, d3d);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
		gFrameCounter++;
	}

	ReleaseGame();
	d3d.ReleaseD3D(true);	
	return 0;
}