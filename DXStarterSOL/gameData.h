#pragma once
#include "score.h"
#include "game.h"
#include <string>


struct GameData
{

  Game game;
  Score score;
  std::string name;
  std::string mssg;
  std::string mssg2;
  std::string mssg3;
  std::string mssg4;
  std::string mssg5;
  int finalScore;
  

  enum class Mode {
    PLAYER_NAME,
    THROW_NUMBER,
    PLAY,
    EXIT
  };
  Mode mode_ = Mode::PLAYER_NAME;


  enum {
    PRINT_OFFSETX = 100,
    PRINT_OFFSETY = 50,
    PRINT_LINEINC = 50,
    PRINT_FONTSIZE = 20
  };

  struct SIXES {
    enum {
      SIDES_DICE = 6,
    };

  };

};
